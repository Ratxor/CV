<?php
    define("LANG", "fr");
    define("ERROR", "Vous utilisez la version 8.0 ou inf&eacute;rieure d'Internet Explorer. Pour des raisons de s&eacute;curit&eacute; et de prise en charge des standards web il est fortement conseill&eacute; d'utiliser Internet Explorer 11, Mozilla Firefox, Google Chrome ou Opera.<br /><br />Toutefois vous pouvez t&eacute;l&eacute;charger mon CV au format PDF en cliquant <a href='/pdf/FR-CV Selmir.pdf'>ici</a>.");
    /*MENU*/
    define("PROFILE", "Profil");
    define("WORK_EXPERIENCE", "Exp&eacute;riences");
    define("EDUCATION", "Scolarit&eacute;");
    define("IT_Interests", "Inter&ecirc;ts");
    define("PORTFOLIO", "Portfolio");
    define("LANGUAGE_SKILLS", "Langues");
    define("INTERESTS", "Centres d'inter&ecirc;ts");
    define("CONTACT", "Contact");

    /*HEADER*/
    define("myNAME", "Selmir Hajruli");
    define("myROLE", "Etudiant en informatique de gestion");
    define("myDESCRIPTION", "Bonjour, je m'appelle Selmir, j'ai 23 ans et je suis un &eacute;tudiant en informatique de gestion. En plus de mes &eacute;tudes, je travaille en tant que technicien helpdesk pour les clients internet de Citycable. Je suis &eacute;galement un d&eacute;veloppeur junior avec des comp&eacute;tences en PHP, HTML/CSS, JavaScript, C#, Java et quelques CMSs comme Joomla, Drupal ou Wordpress.");


    /*PROFILE*/
    define("NAME", "Nom");
    define("BORN", "Naissance");
    define("myDate", "27 d&eacute;cembre 1990");
    define("myBorn", "Bijeljina, BiH");

    /*WORK EXPRIENCE*/
    define("EXPERIENCE_CITYCABLE_DATE", "Novembre 2012 &agrave; pr&eacute;sent");
    define("EXPERIENCE_CITYCABLE_COMPANY", "Citycable");
    define("EXPERIENCE_CITYCABLE_ROLE", "Technicien helpdesk");
    define("EXPERIENCE_CITYCABLE_CITY", " &agrave; Lausanne");
    define("EXPERIENCE_CITYCABLE_DESCRIPTION", "A Citycable je travaille en tant que technicien helpdesk, mon r&ocirc;le est de r&eacute;pondre aux demandes, isoler les probl&egrave;mes et mettre en place des solutions. J'effectue &eacute;galement des configurations de connexion FTTH pour les clients &eacute;ligibles.");
    define("EXPERIENCE_CITYCABLE_QUALITY1", "Communication");
    define("EXPERIENCE_CITYCABLE_QUALITY2", "Fl&eacute;xibilit&eacute;");
    define("EXPERIENCE_CITYCABLE_QUALITY3", "R&eacute;sistance");
    define("EXPERIENCE_CITYCABLE_QUALITY4", "Gestion des conflits");

    define("EXPERIENCE_CITYCABLE_STAGE_DATE", "Ao&ucirc;t 2013 - Novembre 2013");
    define("EXPERIENCE_CITYCABLE_STAGE_COMPANY", "Citycable");
    define("EXPERIENCE_CITYCABLE_STAGE_ROLE", "Stagiaire informaticien");
    define("EXPERIENCE_CITYCABLE_STAGE_CITY", " &agrave; Lausanne");
    define("EXPERIENCE_CITYCABLE_STAGE_DESCRIPTION", "Durant ce stage, j'ai particip&eacute; au projet de lancement d'un nouveau produit (TV+). J'ai notamment &eacute;t&eacute; charg&eacute; de r&eacute;aliser les guides d'utilisation (voir portfolio ci-dessous) pour les clients et j'ai également pris part aux processus de tests du produit.");
    define("EXPERIENCE_CITYCABLE_STAGE_QUALITY1", "Cr&eacute;ativit&eacute;");
    define("EXPERIENCE_CITYCABLE_STAGE_QUALITY2", "D&eacute;veloppement web");
    define("EXPERIENCE_CITYCABLE_STAGE_QUALITY3", "Gestion de projet");

    define("EXPERIENCE_BUNGE_DATE", "Ao&ucirc;t 2013 - Janvier 2012");
    define("EXPERIENCE_BUNGE_COMPANY", "Bunge");
    define("EXPERIENCE_BUNGE_ROLE", "Stagiaire informaticien");
    define("EXPERIENCE_BUNGE_CITY", " &agrave; Gen&egrave;ve");
    define("EXPERIENCE_BUNGE_DESCRIPTION", "Nous &eacute;tions charg&eacute;s d'assurer le support pour 3500 employ&eacute;es en Europe (Londres, Moscou, Barcelone, Paris, Berlin, Lisbonne, etc...) le support se faisait donc en Anglais. De plus, j'ai particip&eacute; &agrave; la migration de XP &agrave; Windows 7. Je m'occupais, notamment de tester les images et configurations Windows 7.");
    define("EXPERIENCE_BUNGE_QUALITY1", "Responsabilit&eacute;");
    define("EXPERIENCE_BUNGE_QUALITY2", "Anglais");
    define("EXPERIENCE_BUNGE_QUALITY3", "Confiance en soi");
    define("EXPERIENCE_BUNGE_QUALITY4", "Esprit d'initiative");

    define("EXPERIENCE_SIL_DATE", "Janvier 2011 - Ao&ucirc;t 2011");
    define("EXPERIENCE_SIL_COMPANY", "SIL");
    define("EXPERIENCE_SIL_ROLE", "Stagiaire informaticien");
    define("EXPERIENCE_SIL_CITY", " &agrave; Lausanne");
    define("EXPERIENCE_SIL_DESCRIPTION", "Durant ce premier stage en tant qu'informaticien je travaillais au sein de l'&eacute;quipe helpdesk de l'entreprise. Etant arriv&eacute; en pleine migration XP -&gt; Windows 7, j'&eacute;tais charg&eacute; d'effectuer les formations des utilisateurs pour le nouveau syst&egrave;me. J'ai donc du r&eacute;aliser des supports de cours et faire une cinquantaine de pr&eacute;sentations devant 10-15 personnes. Cela m'a permis de prendre confiance en moi et d'am&eacute;liorer ma communication.");
    define("EXPERIENCE_SIL_QUALITY1", "Organisation");
    define("EXPERIENCE_SIL_QUALITY2", "Patience");
    define("EXPERIENCE_SIL_QUALITY3", "Pr&eacute;sentation");
    define("EXPERIENCE_SIL_QUALITY4", "Travail d'&eacute;quipe");

    /*EDUCATION*/
    define("EDUCATION_ESVIG_DATE", "Ao&ucirc;t 2012 &agrave; pr&eacute;sent");
    define("EDUCATION_ESVIG_TITLE", "Informaticien de gestion ES");
    define("EDUCATION_ESVIG_SCHOOL", "ETML-ES");
    define("EDUCATION_ESVIG_CITY", " &agrave; Lausanne");
    define("EDUCATION_ESVIG_DESCRIPTION", "J'obtiendrai mon dipl&ocirc;me d'&eacute;cole sup&eacute;rieure d'informaticien de gestion en octobre 2014.");

    define("EDUCATION_CPNV_DATE", "Ao&ucirc;t 2008 - July 2012");
    define("EDUCATION_CPNV_TITLE", "Apprentissage CFC d'informaticien");
    define("EDUCATION_CPNV_SCHOOL", "CPNV");
    define("EDUCATION_CPNV_CITY", " &agrave; Ste-Croix");
    define("EDUCATION_CPNV_DESCRIPTION", "J'ai obtenu mon Certificat F&eacute;d&eacute;ral de Capacit&eacute; d'informaticien en juillet 2012");

    /*IT INTERESTS*/
    define("IT_Java", "Java");
    define("IT_C", "C#");
    define("IT_WEB", "D&eacute;veloppement WEB");
    define("IT_PROJECT", "Gestion de projet");
    define("IT_ITIL", "ITIL");
    define("IT_OPEN", "Technologies open source");
    define("IT_ANDROID", "Android");
    define("IT_VIRTUALIZATION", "Virtualisation");
    define("IT_SECURITY", "S&eacute;curit&eacute;");
    define("IT_SAP", "SAP");
    define("IT_CLOUD", "Cloud computing");

    /*PORTOFOLIO*/
    define("PORTFOLIO_GUIDE_TITRE", "Guide interactif");
    define("PORTFOLIO_GUIDE_LIEU", "TV+ Citycable");
    define("PORTFOLIO_GUIDE_DESCRIPTION", "Voici le guide interactif que j'ai r&eacute;alis&eacute; pour Citycable en HTML/CSS et JavaScript. Cet outil reproduit l'interface du produit TV+ et permet &agrave; l'utilisateur de naviguer dans celui-ci comme si c&rsquo;&eacute;tait le v&eacute;ritable produit afin d'en apprendre un peu plus sur ses fonctionnalit&eacute;s. Cliquez sur l'image pour tester l'outil sur le site de Citycable.");

    define("PORTFOLIO_TICKETS_TITRE", "Syst&egrave;me de gestion de tickets");
    define("PORTFOLIO_TICKETS_LIEU", "Projet de CFC");
    define("PORTFOLIO_TICKETS_DESCRIPTION", "Pour mon travail final en apprentissage CFC d'informaticien j'ai r&eacute;alis&eacute; de A &agrave; Z un syst&egrave;me de gestion de tickets en HTML/CSS et PHP/MYSQL.");

    define("PORTFOLIO_STATS_TITRE", "Selfstats");
    define("PORTFOLIO_STATS_LIEU", "Projet CPNV");
    define("PORTFOLIO_STATS_DESCRIPTION", "R&eacute;alisation en PHP/MYSQL d'un site-web permettant aux personnes autodidactes de se former sur le sujet des statistiques. J'avais du d'abord appprendre les bases en statistiques, puis produire des cours et questions par chapitre pour enfin r&eacute;aliser la partie technique du site. A la fin de chaque chapitre l'utilisateur devait r&eacute;pondre au questionnire et obtenir la moyenne afin de passer au niveau suivant.");

    /*LANGUAGES*/
    define("LANGUAGES_FRENCH", "Fran&ccedil;ais");
    define("LANGUAGES_ENGLISH", "Anglais");
    define("LANGUAGES_GERMAN", "Allemand");
    define("LANGUAGES_BOSNIAN", "Serbo-Croate");

    /*Interests*/
    define("INTERESTS_SOCCER", "Football");
    define("INTERESTS_BASKET", "Basketball");
    define("INTERESTS_TENNIS", "Tennis");
    define("INTERESTS_USFOOTBALL", "Football américain");
    define("INTERESTS_IT", "Informatique");
    define("INTERESTS_MUSIC", "Musique");
    define("INTERESTS_NEWTECH", "Nouvelles technologies");

    /*CONTACT*/
    define("CONTACT_ADRESS", "Adresse");
    define("CONTACT_myADRESS", "Chemin des Rosiers 1");
    define("CONTACT_myADRESS2", "1004 Lausanne");
    define("CONTACT_MOBILE", "Portable");
    define("CONTACT_myMobile", "+41 76 822 00 82");
    define("CONTACT_MAIL", "Email");
    define("CONTACT_myMail", "selmir[at]hajruli.ch");

    
    define("FORM_NAME", "Entrez votre nom");
    define("FORM_MAIL", "Entrez votre email");
    define("FORM_SUBJECT", "Entrez un sujet");
    define("FORM_MESSAGE", "Entrez votre message ici");
    define("FORM_BUTON", " Envoyer");

?>