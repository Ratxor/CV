<?php
define("LANG", "en");
define("ERROR", "You are using Internet Explorer version 8.0 or lower. Due to security issues and lack of support for Web Standards it is highly recommended that you upgrade to a modern browser like Internet Explorer 11, Mozilla Firefox, Google Chrome or Opera.<br /><br />You can download my PDF resume by clicking on <a href='/pdf/EN-CV Selmir.pdf'>this</a> link.");
/*MENU*/
define("PROFILE", "Profile");
define("WORK_EXPERIENCE", "Work experience");
define("EDUCATION", "Education");
define("IT_Interests", "IT Interests");
define("PORTFOLIO", "Portfolio");
define("LANGUAGE_SKILLS", "Language skills");
define("INTERESTS", "Interests");
define("CONTACT", "Contact");

/*HEADER*/
define("myNAME", "Selmir Hajruli");
define("myROLE", "IT Management student");
define("myDESCRIPTION", "Hello, my name is Selmir ! I'm a twenty-three years old IT Student. In addition to my studies I work at Citycable as a technician for internet customers. I'm also a junior developer with skils in PHP, HTML/CSS, JavaScript, C#, Java and some CMSs like Joomla, Drupal or Wordpress.");


/*PROFILE*/
define("NAME", "Name");
define("BORN", "Born");
define("myDate", "27 December 1990");
define("myBorn", "Bijeljina, BiH");

/*WORK EXPRIENCE*/
define("EXPERIENCE_CITYCABLE_DATE", "November 2012 - Current");
define("EXPERIENCE_CITYCABLE_COMPANY", "Citycable");
define("EXPERIENCE_CITYCABLE_ROLE", "IT Helpdesk agent");
define("EXPERIENCE_CITYCABLE_CITY", " in Lausanne, CH");
define("EXPERIENCE_CITYCABLE_DESCRIPTION", "My role is to provide technical assistance and support related to internet connection and TV to Citycable customers. I have to respond to queries, run diagnostic programs, isolate problems, and determine/implement solutions. I also set up FTTH connection for eligible clients.");
define("EXPERIENCE_CITYCABLE_QUALITY1", "Communication");
define("EXPERIENCE_CITYCABLE_QUALITY2", "Adaptability");
define("EXPERIENCE_CITYCABLE_QUALITY3", "Resilience");
define("EXPERIENCE_CITYCABLE_QUALITY4", "Conflict management");

define("EXPERIENCE_CITYCABLE_STAGE_DATE", "August 2013 - November 2013");
define("EXPERIENCE_CITYCABLE_STAGE_COMPANY", "Citycable");
define("EXPERIENCE_CITYCABLE_STAGE_ROLE", "IT Trainee");
define("EXPERIENCE_CITYCABLE_STAGE_CITY", " in Lausanne, CH");
define("EXPERIENCE_CITYCABLE_STAGE_DESCRIPTION", "I've made an interactive guide for a new Citycable product (TV+) in HTML/CSS and JavaScript. I've also made step by step tutorials and a 'Getting started' guide for this product.");
define("EXPERIENCE_CITYCABLE_STAGE_QUALITY1", "Creativity");
define("EXPERIENCE_CITYCABLE_STAGE_QUALITY2", "Web development");
define("EXPERIENCE_CITYCABLE_STAGE_QUALITY3", "Project management");

define("EXPERIENCE_BUNGE_DATE", "August 2011 - January 2012");
define("EXPERIENCE_BUNGE_COMPANY", "Bunge");
define("EXPERIENCE_BUNGE_ROLE", "IT Trainee");
define("EXPERIENCE_BUNGE_CITY", " in Geneva, CH");
define("EXPERIENCE_BUNGE_DESCRIPTION", "I worked with the Helpdesk team that was in charge of IT infrastructure across Europe. I also participated to the migration from Windows XP to Windows 7, I was in charge to test the Windows 7 images and configurations.");
define("EXPERIENCE_BUNGE_QUALITY1", "Responsability");
define("EXPERIENCE_BUNGE_QUALITY2", "English speaking");
define("EXPERIENCE_BUNGE_QUALITY3", "Self-confidence");
define("EXPERIENCE_BUNGE_QUALITY4", "Show initiative");

define("EXPERIENCE_SIL_DATE", "January 2011 - August 2011");
define("EXPERIENCE_SIL_COMPANY", "SIL");
define("EXPERIENCE_SIL_ROLE", "IT Trainee");
define("EXPERIENCE_SIL_CITY", " in Lausanne, CH");
define("EXPERIENCE_SIL_DESCRIPTION", "At start I worked as a helpdesk agent for the SIL staff. After that, I participated to the migration from Windows XP to Windows 7 and I had to train the staff for Windows 7 and Office 2010. I presented lessons for groups of 10 people 2-3 times a week. Yes I was a teacher and I really appreciated it.");
define("EXPERIENCE_SIL_QUALITY1", "Organisation");
define("EXPERIENCE_SIL_QUALITY2", "Patience");
define("EXPERIENCE_SIL_QUALITY3", "Presentation");
define("EXPERIENCE_SIL_QUALITY4", "Teamwork");

/*EDUCATION*/
define("EDUCATION_ESVIG_DATE", "August 2012 - present");
define("EDUCATION_ESVIG_TITLE", "IT Management student");
define("EDUCATION_ESVIG_SCHOOL", "ETML-ES");
define("EDUCATION_ESVIG_CITY", " in Lausanne, CH");
define("EDUCATION_ESVIG_DESCRIPTION", "I will get my PET college degree in October of 2014.");

define("EDUCATION_CPNV_DATE", "August 2008 - July 2012");
define("EDUCATION_CPNV_TITLE", "IT Apprenticeship CFC");
define("EDUCATION_CPNV_SCHOOL", "CPNV");
define("EDUCATION_CPNV_CITY", " in Ste-Croix, CH");
define("EDUCATION_CPNV_DESCRIPTION", "I obtained my Federal VET Diploma in July of 2012.");

/*IT INTERESTS*/
define("IT_Java", "Java");
define("IT_C", "C#");
define("IT_WEB", "Web development");
define("IT_PROJECT", "Project management");
define("IT_ITIL", "ITIL");
define("IT_OPEN", "Open source technologies");
define("IT_ANDROID", "Android");
define("IT_VIRTUALIZATION", "Virtualization");
define("IT_SECURITY", "Security");
define("IT_SAP", "SAP");
define("IT_CLOUD", "Cloud computing");

/*PORTOFOLIO*/
define("PORTFOLIO_GUIDE_TITRE", "Interactive guide");
define("PORTFOLIO_GUIDE_LIEU", "TV+ Citycable");
define("PORTFOLIO_GUIDE_DESCRIPTION", "This is the interactive guide which I've made for Citycable and it's new TV+ product. This tool act like the real TV+ you can click on each item and try it like on the real prdouct. Feel free to click on the image and try it in live.");

define("PORTFOLIO_TICKETS_TITRE", "Helpdesk tickets manager");
define("PORTFOLIO_TICKETS_LIEU", "CPNV Final project");
define("PORTFOLIO_TICKETS_DESCRIPTION", "It's a helpdesk tickets management tool that I've developed for my TPI review in CPNV. The system manages tickets category, priority, attribution, etc. You can also see some stats about the system.");

define("PORTFOLIO_STATS_TITRE", "Selfstats");
define("PORTFOLIO_STATS_LIEU", "CPNV Project");
define("PORTFOLIO_STATS_DESCRIPTION", "It's a self-training tool about stats that I've developed with PHP. I've also created the design whith Photshop and the integration with CSS.");

/*LANGUAGES*/
define("LANGUAGES_FRENCH", "French");
define("LANGUAGES_ENGLISH", "English");
define("LANGUAGES_GERMAN", "German");
define("LANGUAGES_BOSNIAN", "Serbo-Croatian");

/*Interests*/
define("INTERESTS_SOCCER", "Soccer");
define("INTERESTS_BASKET", "Basketball");
define("INTERESTS_TENNIS", "Tennis");
define("INTERESTS_USFOOTBALL", "US Football");
define("INTERESTS_IT", "IT");
define("INTERESTS_MUSIC", "Music");
define("INTERESTS_NEWTECH", "New technologies");

/*CONTACT*/
define("CONTACT_ADRESS", "Adress");
define("CONTACT_myADRESS", "Chemin des Rosiers 1");
define("CONTACT_myADRESS2", "1004 Lausanne");
define("CONTACT_MOBILE", "Mobile");
define("CONTACT_myMobile", "+41 76 822 00 82");
define("CONTACT_MAIL", "Email");
define("CONTACT_myMail", "selmir[at]hajruli.ch");

define("FORM_NAME", "Enter your name");
define("FORM_MAIL", "Enter your email");
define("FORM_SUBJECT", "Enter subject");
define("FORM_MESSAGE", "Enter message here");
define("FORM_BUTON", " Send message");

?>