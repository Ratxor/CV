<?php
SESSION_START();

if (isset($_GET['l'])) {
    if ($_GET['l'] == 'fr') {
        $_SESSION['langue'] = 'fr';
        include('lang/fr.php');
    } elseif ($_GET['l'] == 'en') {
        $_SESSION['langue'] = 'en';
        include('lang/en.php');
    } else {
        include('lang/fr.php');
    }
} else {
    if (isset($_SESSION['langue'])) {
        if ($_SESSION['langue']=='en') {
            include('lang/en.php');
        } else {
            include('lang/fr.php');
        }
    } else {
        include('lang/fr.php');
    }
}



?>
<!DOCTYPE html>

<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->

<html class="not-ie" lang="<?php echo LANG; ?>">
<!--<![endif]-->
    
<head>
<meta charset="utf-8">
<meta name="description" content="description" />
<meta name="keywords" content="keywords"/>
<meta name="author" content="Selmir Hajruli" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
<title>Selmir Hajruli</title>

<!-- google web font-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' rel='stylesheet' type='text/css'>

<!-- style sheets-->
<link rel="stylesheet" media="all" href="css/bootstrap.css" type="text/css"/>
<link rel="stylesheet" media="all" href="css/custom.css" type="text/css"/>
<link rel="stylesheet" media="all" href="font-awesome/css/font-awesome.min.css">




<!-- main jquery libraries / others are at the bottom-->
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/modernizr.js" type="text/javascript"></script>

</head>
<body>
<?php
if(preg_match('/(?i)msie [1-8]/',$_SERVER['HTTP_USER_AGENT']))
{
?>   
    <div class="panel panel-danger">
      <div class="panel-heading">
        <h3 class="panel-title">Internet explorer <=8</h3>
      </div>
      <div class="panel-body">
        <?php echo ERROR; ?>
      </div>
    </div>
<?php
}
else
{
?>
    <div class="settings">
        <span class="glyphicon glyphicon-globe"></span>
        <a href="/index.php?l=en" class="btn top-dark">English</a>
        <a href="/index.php?l=fr" class="btn top-dark">Fran&ccedil;ais</a>
    </div>

    <!-- Navigation -->
    <nav id="spy" class="spy hidden-sm hidden-xs">
        <ul>
            <li class="active"><a href="#profile"><?php echo PROFILE; ?></a></li>
            <li><a href="#workxp"><?php echo WORK_EXPERIENCE; ?></a></li>
            <li><a href="#educ"><?php echo EDUCATION; ?></a></li>
            <li><a href="#itint"><?php echo IT_Interests; ?></a></li>
            <li><a href="#pf"><?php echo PORTFOLIO; ?></a></li>
            <li><a href="#langskills"><?php echo LANGUAGE_SKILLS; ?></a></li>
            <li><a href="#int"><?php echo INTERESTS; ?></a></li>
            <li><a href="#contact"><?php echo CONTACT; ?></a></li>
        </ul>
    </nav>

    <!-- end Navigation -->

    <!-- Navigation for small devices -->
    <nav id="mini-nav" class="spy visible-sm visible-xs">
        <ul>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown">Menu</a>
                <ul class="dropdown-menu">
                    <li class="active"><a href="#profile"><?php echo PROFILE; ?></a></li>
                    <li><a href="#workxp"><?php echo WORK_EXPERIENCE; ?></a></li>
                    <li><a href="#educ"><?php echo EDUCATION; ?></a></li>
                    <li><a href="#itint"><?php echo IT_Interests; ?></a></li>
                    <li><a href="#pf"><?php echo PORTFOLIO; ?></a></li>
                    <li><a href="#langskills"><?php echo LANGUAGE_SKILLS; ?></a></li>
                    <li><a href="#int"><?php echo INTERESTS; ?></a></li>
                    <li><a href="#contact"><?php echo CONTACT; ?></a></li>
                </ul>
            </li>
          </ul>
    </nav>
    <!-- end Navigation for small devices -->

    <!-- Header -->
    <header id="header">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text-center">
                    <img src="img/face.jpg" class="face" alt="">
                    <hr class="empty">
                    <p class="header-name"><?php echo myNAME; ?></p>
                    <p class="header-title"><?php echo myROLE; ?></p>
                    <p class="header-subtitle"><b><?php echo myDESCRIPTION; ?></b></p>
                </div>
            </div>
        </div>
    </header>
    <!-- end Header -->

    <!-- Profile -->
    <section id="profile" class="profile activate">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 col-sm-offset-5">
                    <ul>
                        <li class="title"><span class="glyphicon glyphicon-user"></span><?php echo PROFILE; ?></li>
                        <li>
                            <span class="note"><?php echo NAME; ?></span>
                            <p><strong><?php echo myNAME; ?></strong></p>
                        </li>
                        <li>
                            <span class="note"><?php echo BORN; ?></span>
                            <p><strong><?php echo myDate; ?></strong></p>
                            <p><strong><?php echo myBorn; ?></strong></p>
                        </li>

                        <li>
                            <a href="https://www.linkedin.com/in/selmir"><button class="btn btn-linkedin"><i class="fa fa-linkedin"></i></button></a>
                            <a href="https://google.com/+SelmirHajruli"><button class="btn btn-google-plus"><i class="fa fa-google-plus"></i></button></a>
                            <a href="https://twitter.com/Selmir_Hajruli"><button class="btn btn-twitter"><i class="fa fa-twitter"></i></button></a>
                            <a href="https://www.facebook.com/selmir.hajruli"><button class="btn btn-facebook"><i class="fa fa-facebook"></i></button></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- end Profile -->

    <!-- Work experience -->
    <section id="work-experience" class="work-experience">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 col-sm-offset-5">
                    <ul>
                        <li class="title"><span id="workxp" class="glyphicon glyphicon-briefcase"></span><?php echo WORK_EXPERIENCE; ?></li>
                        <li>
                            <span class="note"><?php echo EXPERIENCE_CITYCABLE_DATE; ?></span>
                            <p><strong><?php echo EXPERIENCE_CITYCABLE_COMPANY; ?></strong></p>
                            <p><strong><?php echo EXPERIENCE_CITYCABLE_ROLE; ?></strong><?php echo EXPERIENCE_CITYCABLE_CITY; ?></p>
                            <p class="description"><?php echo EXPERIENCE_CITYCABLE_DESCRIPTION; ?></p>
                            <p>
                                <span class="label label-primary"><?php echo EXPERIENCE_CITYCABLE_QUALITY1; ?></span>
                                <span class="label label-primary"><?php echo EXPERIENCE_CITYCABLE_QUALITY2; ?></span>
                                <span class="label label-primary"><?php echo EXPERIENCE_CITYCABLE_QUALITY3; ?></span>
                                <span class="label label-primary"><?php echo EXPERIENCE_CITYCABLE_QUALITY4; ?></span>
                            </p>
                        </li>
                        <li>
                            <span class="note"><?php echo EXPERIENCE_CITYCABLE_STAGE_DATE; ?></span>
                            <p><strong><?php echo EXPERIENCE_CITYCABLE_STAGE_COMPANY; ?></strong></p>
                            <p><strong><?php echo EXPERIENCE_CITYCABLE_STAGE_ROLE; ?></strong><?php echo EXPERIENCE_CITYCABLE_STAGE_CITY; ?></p>
                            <p class="description"><?php echo EXPERIENCE_CITYCABLE_STAGE_DESCRIPTION; ?></p>
                            <p>
                                <span class="label label-primary"><?php echo EXPERIENCE_CITYCABLE_STAGE_QUALITY1; ?></span>
                                <span class="label label-primary"><?php echo EXPERIENCE_CITYCABLE_STAGE_QUALITY2; ?></span>
                                <span class="label label-primary"><?php echo EXPERIENCE_CITYCABLE_STAGE_QUALITY3; ?></span>
                            </p>
                        </li>
                        <li>
                            <span class="note"><?php echo EXPERIENCE_BUNGE_DATE; ?></span>
                            <p><strong><?php echo EXPERIENCE_BUNGE_COMPANY; ?></strong></p>
                            <p><strong><?php echo EXPERIENCE_BUNGE_ROLE; ?></strong><?php echo EXPERIENCE_BUNGE_CITY; ?></p>
                            <p class="description"><?php echo EXPERIENCE_BUNGE_DESCRIPTION; ?></p>
                            <p>
                                <span class="label label-primary"><?php echo EXPERIENCE_BUNGE_QUALITY1; ?></span>
                                <span class="label label-primary"><?php echo EXPERIENCE_BUNGE_QUALITY2; ?></span>
                                <span class="label label-primary"><?php echo EXPERIENCE_BUNGE_QUALITY3; ?></span>
                                <span class="label label-primary"><?php echo EXPERIENCE_BUNGE_QUALITY4; ?></span>
                            </p>
                        </li>
                        <li>
                            <span class="note"><?php echo EXPERIENCE_SIL_DATE; ?></span>
                            <p><strong><?php echo EXPERIENCE_SIL_COMPANY; ?></strong></p>
                            <p><strong><?php echo EXPERIENCE_SIL_ROLE; ?></strong><?php echo EXPERIENCE_SIL_CITY; ?></p>
                            <p class="description"><?php echo EXPERIENCE_SIL_DESCRIPTION; ?></p>
                            <p>
                                <span class="label label-primary"><?php echo EXPERIENCE_SIL_QUALITY1; ?></span>
                                <span class="label label-primary"><?php echo EXPERIENCE_SIL_QUALITY2; ?></span>
                                <span class="label label-primary"><?php echo EXPERIENCE_SIL_QUALITY3; ?></span>
                                <span class="label label-primary"><?php echo EXPERIENCE_SIL_QUALITY4; ?></span>
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- end Work experience -->

    <!-- Education -->
    <section id="education" class="education">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 col-sm-offset-5">
                    <ul>
                        <li class="title"><span id="educ" class="glyphicon glyphicon-book"></span><?php echo EDUCATION; ?></li>
                        <li>
                            <span class="note"><?php echo EDUCATION_ESVIG_DATE; ?></span>
                            <p><strong><?php echo EDUCATION_ESVIG_TITLE; ?></strong></p>
                            <p><strong><?php echo EDUCATION_ESVIG_SCHOOL; ?></strong><?php echo EDUCATION_ESVIG_CITY; ?></p>
                            <p class="description"><?php echo EDUCATION_ESVIG_DESCRIPTION; ?></p>
                        </li>
                        <li>
                            <span class="note"><?php echo EDUCATION_CPNV_DATE; ?></span>
                            <p><strong><?php echo EDUCATION_CPNV_TITLE; ?></strong></p>
                            <p><strong><?php echo EDUCATION_CPNV_SCHOOL; ?></strong><?php echo EDUCATION_CPNV_CITY; ?></p>
                            <p class="description"><?php echo EDUCATION_CPNV_DESCRIPTION; ?></p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- end Education -->

    <!-- Professional skills -->
    <section id="professional-skills" class="professional-skills">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 col-sm-offset-5">
                    <ul>
                        <li class="title"><span id="itint" class="glyphicon glyphicon-screenshot"></span><?php echo IT_Interests; ?></li>
                        <li>
                            <p><?php echo IT_Java; ?><span class="annotation pull-right">95%</span></p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </li>
                        <li>
                            <p><?php echo IT_C; ?><span class="annotation pull-right">90%</span></p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </li>
                        <li>
                            <p><?php echo IT_WEB; ?><span class="annotation pull-right">85%</span></p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </li>
                        <li>
                            <p><?php echo IT_PROJECT; ?><span class="annotation pull-right">80%</span></p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </li>
                        <li>
                            <p>
                                <span class="label label-primary"><?php echo IT_ITIL; ?></span>
                                <span class="label label-success"><?php echo IT_OPEN; ?></span>
                                <span class="label label-info"><?php echo IT_ANDROID; ?></span>
                                <span class="label label-warning"><?php echo IT_VIRTUALIZATION; ?></span>
                                <span class="label label-danger"><?php echo IT_SECURITY; ?></span>
                                <span class="label label-primary"><?php echo IT_SAP; ?></span>
                                <span class="label label-info"><?php echo IT_CLOUD; ?></span>
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- end Professional skills -->
    <!-- Portfolio -->
    <section id="portfolio" class="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 col-sm-offset-5">
                    <ul>
                        <li class="title"><span id="pf" class="glyphicon glyphicon-folder-open"></span><?php echo PORTFOLIO; ?></li>
                        <li>
                            <span class="note"><?php echo PORTFOLIO_GUIDE_TITRE; ?></span>
                            <p><strong><?php echo PORTFOLIO_GUIDE_LIEU; ?></strong></p>
                            <a href="http://home.citycable.ch/tvplus/"><img src="img/tvplus.png" alt="tvplus" /> </a>
                            <p class="description"><?php echo PORTFOLIO_GUIDE_DESCRIPTION; ?></p>
                        </li>
                        <li>
                            <span class="note"><?php echo PORTFOLIO_TICKETS_TITRE; ?></span>
                            <p><strong><?php echo PORTFOLIO_TICKETS_LIEU; ?></strong></p>
                            <img src="img/helpdesk.png" alt="helpdesk" />
                            <p class="description"><?php echo PORTFOLIO_TICKETS_DESCRIPTION; ?></p>
                        </li>
                        <li>
                            <span class="note"><?php echo PORTFOLIO_STATS_TITRE; ?></span>
                            <p><strong><?php echo PORTFOLIO_STATS_LIEU; ?></strong></p>
                            <img src="img/selfstats.png" alt="selfstats" />
                            <p class="description"><?php echo PORTFOLIO_STATS_DESCRIPTION; ?></p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section id="personal-skills" class="personal-skills">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 col-sm-offset-5">
                    <ul>
                        <li class="title"><span id="langskills" class="glyphicon glyphicon-globe"></span><?php echo LANGUAGE_SKILLS; ?></li>
                        <li>
                            <span class="chart" data-percent="100"><span class="skill"><?php echo LANGUAGES_FRENCH; ?></span>
                                <span class="percent"></span>
                            </span>
                            <span class="chart" data-percent="60"><span class="skill"><?php echo LANGUAGES_ENGLISH; ?></span>
                                <span class="percent"></span>
                            </span>
                            <span class="chart" data-percent="20"><span class="skill"><?php echo LANGUAGES_GERMAN; ?></span>
                                <span class="percent"></span>
                            </span>
                            <span class="chart" data-percent="80"><span class="skill"><?php echo LANGUAGES_BOSNIAN; ?></span>
                                <span class="percent"></span>
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- Interests -->
    <section id="interests" class="interests">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 col-sm-offset-5">
                    <ul>
                        <li class="title"><span id="int" class="glyphicon glyphicon-heart-empty"></span><?php echo INTERESTS; ?></li>
                        <li>
                            <p>
                                <span class="label label-danger"><?php echo INTERESTS_SOCCER; ?></span>
                                <span class="label label-danger"><?php echo INTERESTS_BASKET; ?></span>
                                <span class="label label-danger"><?php echo INTERESTS_TENNIS; ?></span>
                                <span class="label label-danger"><?php echo INTERESTS_USFOOTBALL; ?></span>
                                <span class="label label-danger"><?php echo INTERESTS_IT; ?></span>
                                <span class="label label-danger"><?php echo INTERESTS_MUSIC; ?></span>
                                <span class="label label-danger"><?php echo INTERESTS_NEWTECH; ?></span>
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- end Interests -->


    <!-- end Portfolio -->

    <!-- Contact -->
    <section id="contact" class="contact">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 col-sm-offset-5">
                    <ul>
                        <li class="title"><span class="glyphicon glyphicon-envelope"></span><?php echo CONTACT; ?></li>
                        <li class="static">
                            <span class="note"><?php echo CONTACT_ADRESS; ?></span>
                            <p><strong><?php echo CONTACT_myADRESS; ?></strong></p>
                            <p><strong><?php echo CONTACT_myADRESS2; ?></strong></p>
                            <span class="note"><?php echo CONTACT_MOBILE; ?></span>
                            <p><strong><?php echo CONTACT_myMobile; ?></strong></p>
                            <span class="note"><?php echo CONTACT_MAIL; ?></span>
                            <p><strong><?php echo CONTACT_myMail; ?></strong></p>
                            <hr class="empty">
                            
                            <!--
                            <form action="index.php#contact" method="post">
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                    <input name="yourname" type="text" class="form-control" id="inputname" placeholder="<?php echo FORM_NAME; ?>">
                                </div>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
                                    <input name="email" type="email" class="form-control" id="inputemail" placeholder="<?php echo FORM_MAIL; ?>">
                                </div>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-bold"></span></span>
                                    <input name="subject" type="text" class="form-control" id="inputsubject" placeholder="<?php echo FORM_SUBJECT; ?>">
                                </div>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-comment"></span></span>
                                    <textarea name="comments" class="form-control" id="inputmessage" rows="3" placeholder="<?php echo FORM_MESSAGE; ?>"></textarea>
                                </div>
                                <button id="send" name="send" type="submit" class="btn btn-success pull-right"><span class="glyphicon glyphicon-send"></span><?php echo FORM_BUTON; ?></button>
                            </form>
                            <!-- end Contact -->
                        </li>
                        <li class="title up"><span class="glyphicon glyphicon-arrow-up"></span>Up</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    

    <!-- Java Script -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-progressbar.js"></script>
    <script src="js/spy.js"></script>
    <script src="js/scripts.js"></script>
    <!-- Charts -->
    <script src="js/jquery.easypiechart.js"></script>
    <script src="js/chart.js"></script>
<?php
}
?>
</body>

<?php
/*
   if (isset( $_POST['send'] ) ) {
        $emailFrom = "hajruli.s@gmail.com";
        $emailTo = "hajruli.s@gmail.com";
        $name = Trim(stripslashes($_POST['yourname'])); 
        $subject = Trim(stripslashes($_POST['subject'])); 
        $email = Trim(stripslashes($_POST['email'])); 
        $message = Trim(stripslashes($_POST['comments'])); 

        // validation
        $validationOK=true;
        if (!$validationOK) {

          exit;
        }

        // prepare email body text
        $body = "";
        $body .= "Name: ";
        $body .= $name;
        $body .= "\n";
        $body .= "Email: ";
        $body .= $email;
        $body .= "\n";
        $body .= "Message: ";
        $body .= $message;
        $body .= "\n";

        // send email 
        
        $success = mail($emailTo, $subject, $body, "selmir@hajruli.ch");

        // redirect to success page 
        if ($success){
          print "grand sucess";
        }
        else{
          print "lol";
        }
   }
   */
?>

</html>

